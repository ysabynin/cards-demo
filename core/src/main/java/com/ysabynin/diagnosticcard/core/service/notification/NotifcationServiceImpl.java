package com.ysabynin.diagnosticcard.core.service.notification;

import com.ysabynin.diagnosticcard.core.config.Messages;
import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import com.ysabynin.diagnosticcard.core.pojo.CardCreationMode;
import com.ysabynin.diagnosticcard.core.utils.RequestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.text.MessageFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Service
@Slf4j
public class NotifcationServiceImpl implements NotificationService {

    @Value("${mail.managers}")
    String managerEmail;

    @Value("${mail.default.url}")
    String defaultURL;

    @Autowired
    EmailService emailService;

    @Autowired
    Messages messages;

    //TODO: replace text from message.properties to template-driven approach from Apache FreeMarker
    @Override
    public void notifyOwners(Card card, Map<String, String> cardLabelValueMap, String errorMessage) {
        StringBuilder builder = new StringBuilder("Карта не прошла по следующей причине:\n\n");
        if (errorMessage != null)
            builder.append(errorMessage);

        builder.append("\n\nДанные не пройденной заявки:\n\n");
        builder.append(card);
        if (!cardLabelValueMap.isEmpty())
            builder.append("\n\nДанные введенные на сайте:\n");
        for (Map.Entry<String, String> entry : cardLabelValueMap.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append(" ,\n");
        }

        emailService.sendSimpleMessage(managerEmail,
                "Диагностическая карта: ошибка в работе",
                builder.toString());
        log.info("Error message during processing has been sent: " + managerEmail);

    }

    @Override
    public void notifyClient(Card card, String pathToFile) {
        DomainConfig domainConfig = card.getDomainConfig();
        String baseURL = domainConfig == null || StringUtils.isEmpty(domainConfig.getDomain()) ? defaultURL
                : domainConfig.getDomain();
        String message = MessageFormat.format(messages.get("mail.successful.operation"), baseURL, String.valueOf(card.getId()));
        emailService.sendComplexMessage(card.getOwnerEmail(), "Диагностическая карта", message, pathToFile);
        log.info("Card is delievered to the client: " + card.getOwnerEmail());
    }

    @Override
    public void notifyAboutSuccessfullyProcessedCard(Map<String, String> cardLabelValueMap, String pathToFile) {
        StringBuilder builder = new StringBuilder("Данные клиента:\n\n");
        for (Map.Entry<String, String> entry : cardLabelValueMap.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }

        emailService.sendComplexMessage(managerEmail, "Диагностическая карта: заявка выполнена", builder.toString(), pathToFile);
        log.info("Sending a message to owners about successful operation: " + managerEmail);
    }

    public void notifyManagerAboutApplication(Card card, CardCreationMode creationMode) {
        StringBuilder builder = new StringBuilder();
        String subject = "";
        if (creationMode == CardCreationMode.CONFIRM) {
            subject = "Диагностическая карта: подтвердите заявку";
            builder.append("Клиент нажал на кнопку 'Оформить'.\nРежим обработки карты: подтверждение менеджера.\n" +
                    "Заявка в AMO: https://oformlenie.amocrm.ru/leads/detail/" + card.getDealId() + "\n" +
                    "Необходимо ему позвонить и подтвердить заявку по номеру:" + card.getOwnerPhone() + "\n" +
                    "После того, как клиент подтвердит заявку нажмите на ссылку создания карты: " +
                    RequestUtils.getConfirmAddress(card) + "\n" +
                    "Данные клиента:\n\n");
        } else {
            subject = "Диагностическая карта: получена новая заяка";
            builder.append("Клиент нажал на кнопку 'Оформить'. Его заявка будет обработана автоматически после ввода кода подтверждения.\n" +
                    "Для создания карты вручную нажмите на ссылку:" + RequestUtils.getConfirmAddress(card) + ".\n" +
                    "Данные клиента:\n\n");
        }
        builder.append(card);
        emailService.sendSimpleMessage(managerEmail, subject, builder.toString());
        log.info("Sending a message to manager about application: " + managerEmail);
    }

    @Override
    public void notifyManagerAboutUnprocessedCard(Card card, String errorMessage) {
        StringBuilder builder = new StringBuilder("Карта не прошла по следующей причине:\n\n");
        if (errorMessage != null)
            builder.append(errorMessage);

        builder.append("\n\nДанные не пройденной заявки:\n\n");
        builder.append(card);
        emailService.sendSimpleMessage(managerEmail,
                "Диагностическая карта: заявка не обработана",
                builder.toString());
        log.info("Sending a message to manager about unsuccessful operation: " + managerEmail);
    }

    @Override
    public void notifyClientAboutLongTimeProcessedCard(Card card) {
        String message;
        LocalTime time = LocalTime.now();
        if (time.isAfter(LocalTime.of(0, 0)) && (time.isBefore(LocalTime.of(8, 0)))) {
            message = messages.get("mail.client.error.night.operation");
        } else {
            message = messages.get("mail.client.long.operation");
        }
        emailService.sendComplexMessage(card.getOwnerEmail(), "Диагностическая карта", message, null);
        log.info("Sending a message to client about long running operation: " + card.getOwnerEmail() +
                ", Current time: " + time.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
    }

    @Override
    public void notifyManagerAboutLongTimeProcessedCard(Map<String, String> cardLabelValueMap) {
        StringBuilder builder = new StringBuilder(messages.get("mail.manager.long.operation"));
        for (Map.Entry<String, String> entry : cardLabelValueMap.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }

        emailService.sendSimpleMessage(managerEmail,
                "Диагностическая карта: заявка не обработана",
                builder.toString());
        log.info("Sending a message to manager about unsuccessful operation: " + managerEmail);
    }

    @Override
    public void notifyManagerAboutCallback(Map<String, String> callbackData) {
        StringBuilder builder = new StringBuilder("Данные клиента:\n\n");
        for (Map.Entry<String, String> entry : callbackData.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }

        emailService.sendSimpleMessage(managerEmail, "Заказан обратный звонок", builder.toString());
        log.info("Sending a message to manager about callback request: " + managerEmail);
    }

    @Override
    public void notifyManagerAboutPayment(Map<String, String> paymentData) {
        StringBuilder builder = new StringBuilder("Данные оплаты:\n\n");
        for (Map.Entry<String, String> entry : paymentData.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }

        emailService.sendSimpleMessage(managerEmail, "Клиент оплатил карту", builder.toString());
        log.info("Sending a message to manager about payment operation: " + managerEmail);
    }

    @Override
    public void notifyManagerAboutQuestion(Map<String, String> callbackData) {
        StringBuilder builder = new StringBuilder("Данные клиента:\n\n");
        for (Map.Entry<String, String> entry : callbackData.entrySet()) {
            builder.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }

        emailService.sendSimpleMessage(managerEmail,
                "У клиента возник вопрос", builder.toString());
        log.info("Sending a message to manager about question: " + managerEmail);
    }

    @Override
    public void notifyDeveloperAboutUnparsedCities() {
        emailService.sendSimpleMessage(managerEmail,
                "Ошибка в обновлении списка городов", messages.get("mail.manager.error.cities"));
        log.info("Sending a message to manager about not parsed citites: " + managerEmail);
    }
}
