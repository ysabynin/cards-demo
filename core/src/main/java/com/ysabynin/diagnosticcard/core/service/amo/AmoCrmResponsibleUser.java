package com.ysabynin.diagnosticcard.core.service.amo;

public enum AmoCrmResponsibleUser {
    DCARD1("2280784"),
    DCARD2("2280892");

    private String id;

    AmoCrmResponsibleUser(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
