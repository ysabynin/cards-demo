package com.ysabynin.diagnosticcard.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Card extends Auditable implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ownerType;
    private String ownerCompanyName;
    private String ownerName;
    private String ownerSurname;
    private String ownerPatronymic;
    private String ownerPhone;
    private String ownerEmail;
    private String ownerCity;

    private String documentType;
    private String documentSeries;
    private String documentNumber;
    private String documentIssuedDate;
    private String documentIssuedBy;

    private String tsCategory;
    private String tsBCategoryType;
    private String tsRegNumber;
    private String tsVin;
    private String tsBody;
    private String tsFrame;

    private String tsMark;
    private String tsModel;
    private String tsYear;

    private Integer tsMassBase;
    private Integer tsMassMax;
    private Integer tsMileage;

    private String tsFuelType;
    private String tsBrakesType;
    private String tsWheelModel;
    private String tiresId;

    private String tsTaxi;
    private String tsTraining;
    private String tsDangerous;
    private String tsDualFuel;

    private Integer price;
    private Integer dealId;
    private Integer confirmCode;
    private CardStatus status = CardStatus.CREATED;

    @OneToOne
    private DomainConfig domainConfig;
}

