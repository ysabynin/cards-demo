package com.ysabynin.diagnosticcard.core.service.amo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.service.ParameterService;
import com.ysabynin.diagnosticcard.core.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class DealJsonService extends BaseAmoService {
    @Autowired
    ContactJsonService contactJsonService;

    @Autowired
    ParameterService parameterService;

    private String prepareAddDeal(Integer contactId, AmoCrmResponsibleUser user, Card card,
                                  String roistatVisit, HttpServletRequest request) {
        JsonObject rootElement = new JsonObject();
        JsonArray dealArray = new JsonArray();
        rootElement.add("add", dealArray);

        JsonObject dealElement = new JsonObject();
        dealElement.addProperty("name", "Диагностическая карта");
        dealElement.addProperty("created_at", LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(3)));
        dealElement.addProperty("updated_at", LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(3)));
        dealElement.addProperty("responsible_user_id", "2274931");
        dealElement.addProperty("created_by", "2274931");
        dealElement.addProperty("status_id", DealStep.FIRST_APPEAL.getId());
        dealElement.addProperty("sale", card.getPrice());
        JsonArray contacts = new JsonArray();
        contacts.add(contactId);
        dealElement.add("contacts_id", contacts);
        dealElement.addProperty("tags", user.name());

        String customFields = "[" +
                "{\"id\":339105,\"values\":[{\"value\":\"" + roistatVisit + "\"}]}," +
                "{\"id\":378155,\"values\":[{\"value\":\"" + parameterService.getAppMode() + "\"}]}," +
                "{\"id\":359909,\"values\":[{\"value\":\"" + RequestUtils.getUserAgent(request) + "\"}]}," +
                "{\"id\":359911,\"values\":[{\"value\":\"" + RequestUtils.getClientIpAddr(request) + "\"}]}," +
                "{\"id\":359913,\"values\":[{\"value\":\"" + RequestUtils.getClientOS(request) + "\"}]}," +
                "{\"id\":359915,\"values\":[{\"value\":\"" + RequestUtils.getClientBrowser(request) + "\"}]}," +
                "{\"id\":368255,\"values\":[{\"value\":\"" + RequestUtils.getConfirmAddress(card) + "\"}]}" +
                "]";
        dealElement.add("custom_fields", new Gson().fromJson(customFields, JsonArray.class));
        dealArray.add(dealElement);

        return new Gson().toJson(rootElement);
    }

    public Integer addDeal(Card card, AmoCrmResponsibleUser user, String roistatVisit,
                           HttpServletRequest request) {
        ResponseEntity authRespEntity = login();
        List<String> cookies = authRespEntity.getHeaders().get("Set-Cookie");
        HttpHeaders httpHeaders = new HttpHeaders();
        cookies.forEach(cookie -> httpHeaders.add("Cookie", cookie));
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity responseEntity = restTemplate.exchange(
                API_PREFIX + "api/v2/contacts",
                HttpMethod.POST,
                new HttpEntity<>(contactJsonService.prepareContact(card, user), httpHeaders),
                Object.class);

        Integer contactId = JsonPath.read(new Gson().toJson(responseEntity.getBody()), "$._embedded.items[0].id");
        HttpEntity<String> requestEntity = new HttpEntity<>(prepareAddDeal(contactId, user, card, roistatVisit, request), httpHeaders);
        ResponseEntity leadResponseEntity = restTemplate.exchange(
                API_PREFIX + "api/v2/leads",
                HttpMethod.POST,
                requestEntity,
                Object.class);
        return JsonPath.read(new Gson().toJson(leadResponseEntity.getBody()), "$._embedded.items[0].id");
    }

    public ResponseEntity updateDealStep(Card card, Double price, DealStep dealStep) {
        ResponseEntity authRespEntity = login();
        List<String> cookies = authRespEntity.getHeaders().get("Set-Cookie");
        HttpHeaders httpHeaders = new HttpHeaders();
        cookies.forEach(cookie -> httpHeaders.add("Cookie", cookie));
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");

        JsonObject rootElement = new JsonObject();
        JsonArray dealArray = new JsonArray();
        rootElement.add("update", dealArray);

        JsonObject dealElement = new JsonObject();
        dealElement.addProperty("id", card.getDealId());
        dealElement.addProperty("sale", price);
        dealElement.addProperty("updated_at", LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(3)));
        dealElement.addProperty("status_id", dealStep.getId());
        dealArray.add(dealElement);
        String json = new Gson().toJson(rootElement);

        HttpEntity<String> requestEntity = new HttpEntity<>(json, httpHeaders);
        return restTemplate.exchange(
                API_PREFIX + "api/v2/leads",
                HttpMethod.POST,
                requestEntity,
                Object.class);
    }
}
