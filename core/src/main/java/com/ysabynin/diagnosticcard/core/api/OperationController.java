package com.ysabynin.diagnosticcard.core.api;

import com.ysabynin.diagnosticcard.core.config.Messages;
import com.ysabynin.diagnosticcard.core.entity.Account;
import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.entity.CardStatus;
import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import com.ysabynin.diagnosticcard.core.pojo.CardCreationMode;
import com.ysabynin.diagnosticcard.core.pojo.CreateCardTask;
import com.ysabynin.diagnosticcard.core.process.AppMode;
import com.ysabynin.diagnosticcard.core.repository.CardRepository;
import com.ysabynin.diagnosticcard.core.service.DefaultAccountService;
import com.ysabynin.diagnosticcard.core.service.DomainService;
import com.ysabynin.diagnosticcard.core.service.ParameterService;
import com.ysabynin.diagnosticcard.core.service.amo.AmoCrmResponsibleUser;
import com.ysabynin.diagnosticcard.core.service.amo.DealJsonService;
import com.ysabynin.diagnosticcard.core.service.notification.NotificationService;
import com.ysabynin.diagnosticcard.core.service.sms.SmscService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Controller
@PropertySource("classpath:application.properties")
@Slf4j
public class OperationController {
    @Autowired
    AmqpTemplate rabbitTemplate;

    @Autowired
    NotificationService notificationService;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    ParameterService parameterService;

    @Autowired
    DealJsonService dealJsonService;

    @Autowired
    DomainService domainService;

    @Autowired
    SmscService smscService;

    @Autowired
    DefaultAccountService defaultAccountService;

    @Autowired
    Messages messages;

    private void sendApplicationToQueue(Card card, HttpServletRequest request) {
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        AppMode appMode = parameterService.getAppMode();
        Account account = domainConfig != null ? domainConfig.getAccount() : defaultAccountService.getDefaultAccount(appMode);

        CreateCardTask createCardTask = new CreateCardTask(card, account, appMode);
        rabbitTemplate.convertAndSend("spring-boot", createCardTask);
        log.info("Prepared message: " + createCardTask);
    }

    private void prepareCard(Card card, HttpServletRequest request, String roistatVisit) {
        Random rand = new Random();
        int num = rand.nextInt(9000) + 1000;
        card.setConfirmCode(num);
        //TODO: implement angularjs directive, which transforms text to uppercase
        card.setOwnerName(card.getOwnerName().toUpperCase());
        card.setOwnerSurname(card.getOwnerSurname().toUpperCase());
        card.setOwnerPatronymic(card.getOwnerPatronymic().toUpperCase());
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        card.setDomainConfig(domainConfig);
        Integer dealId = dealJsonService.addDeal(card, AmoCrmResponsibleUser.DCARD1, roistatVisit, request);
        card.setDealId(dealId);
    }

    @ResponseBody
    @RequestMapping(value = "/api/manualConfirm/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> checkClient(@PathVariable(value = "id") Long id,
                                         HttpServletRequest request) {
        Card card = cardRepository.findOne(id);
        card.setStatus(CardStatus.VERIFIED);
        sendApplicationToQueue(card, request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/api/verifyClient/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> checkClient(@PathVariable(value = "id") Long id,
                                         @RequestParam(value = "confirmCode", required = false) Integer confirmCode,
                                         HttpServletRequest request) {
        CardCreationMode cardCreationMode = parameterService.getCardCreationMode();
        if (cardCreationMode.equals(CardCreationMode.CONFIRM))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Card card = cardRepository.findOne(id);
        boolean passedVerification = confirmCode.equals(card.getConfirmCode());
        log.info("User verification code = " + confirmCode + " , Correct is " + card.getConfirmCode() + ", Result =" + passedVerification);
        if (passedVerification) {
            card.setStatus(CardStatus.VERIFIED);
            sendApplicationToQueue(card, request);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @ResponseBody
    @RequestMapping(value = "/api/launch", method = RequestMethod.POST)
    public Long saveApplication(HttpServletRequest request,
                                @RequestBody Card card,
                                @CookieValue(value = "roistat_visit", required = false) String roistatVisit) {
        Date today = Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Card duplicatedCard = cardRepository.findOneByTsVinAndTsBodyAndTsFrameAndCreationDate(card.getTsVin(), card.getTsBody(), card.getTsFrame(), today);
        if (duplicatedCard != null) {
            return null;
        }

        log.info("Card price should be =" + card.getPrice() + ", ROISTAT_VISIT = " + roistatVisit);
        prepareCard(card, request, roistatVisit);
        Card savedCard = cardRepository.save(card);
        CardCreationMode cardCreationMode = parameterService.getCardCreationMode();
        notificationService.notifyManagerAboutApplication(savedCard, cardCreationMode);

        if (cardCreationMode.equals(CardCreationMode.AUTO)) {
            card.setStatus(CardStatus.VERIFIED);
            sendApplicationToQueue(card, request);
        }
        return savedCard.getId();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/performCallback", method = RequestMethod.POST)
    public void performCallback(@RequestBody Map<String, String> callbackData) {
        notificationService.notifyManagerAboutCallback(callbackData);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/sendQuestion", method = RequestMethod.POST)
    public void sendQuestion(@RequestBody Map<String, String> callbackData) {
        notificationService.notifyManagerAboutQuestion(callbackData);
    }
}
