package com.ysabynin.diagnosticcard.core.api.payment;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class YandexUtils implements Serializable {
    private static final String[] EXPECTED_PARAMS_ARRAY = new String[]{
            "notification_type",
            "operation_id",
            "amount",
            "currency",
            "datetime",
            "sender",
            "codepro",
            "notification_secret",
            "label"};

    private static final String DELIMITER = "&";

    private static void checkAllParametersNotNull(Map<String, String> map) {
        for (String s : EXPECTED_PARAMS_ARRAY) {
            if (!map.containsKey(s)) {
                throw new IllegalArgumentException("param " + s + " is absent");
            }
        }
    }

    private static String calculateHash(Map<String, String> parameterMap) {
        String stringForHash = createStringForHash(parameterMap);
        return Hex.encodeHexString(DigestUtils.sha1(stringForHash));
    }

    private static String createStringForHash(Map<String, String> parameterMap) {
        List<String> strings = new ArrayList<>();
        for (String paramName : EXPECTED_PARAMS_ARRAY) {
            strings.add(parameterMap.get(paramName));
        }

        StringBuilder stringForHash = new StringBuilder(strings.get(0));
        for (String param : strings.subList(1, strings.size())) {
            stringForHash.append(DELIMITER).append(param);
        }
        return stringForHash.toString();
    }

    public static boolean isHashValid(Map<String, String> parameterMap, String secret) {
        Map<String, String> map = new HashMap<>(parameterMap);
        map.put("notification_secret", secret);

        checkAllParametersNotNull(map);

        String realHash = calculateHash(map);
        String sha1HashParam = map.get("sha1_hash");

        boolean equals = realHash.equalsIgnoreCase(sha1HashParam);
        if (!equals) {
            log.error("the hashes are not equals. expected: " + realHash + ", but received: " + sha1HashParam);
        }
        return equals;
    }
}
