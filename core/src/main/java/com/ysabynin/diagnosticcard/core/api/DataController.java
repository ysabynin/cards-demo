package com.ysabynin.diagnosticcard.core.api;

import com.ysabynin.diagnosticcard.core.pojo.ComboboxItem;
import com.ysabynin.diagnosticcard.core.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DataController {
    @Autowired
    CityService cityService;

    @RequestMapping(value = "/api/cities", method = RequestMethod.GET)
    public List<ComboboxItem> getCities() {
        return cityService.getOptions();
    }
}
