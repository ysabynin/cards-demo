package com.ysabynin.diagnosticcard.core.service.amo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ysabynin.diagnosticcard.core.entity.Card;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class ContactJsonService extends BaseAmoService {
    private JsonElement getJsonElementWithNameAndValue(Integer id, Integer enumId, String value) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", id);
        jsonObject.addProperty("is_system", true);
        JsonArray valuesArray = new JsonArray();
        JsonObject valueObject = new JsonObject();
        valueObject.addProperty("value", value);
        valueObject.addProperty("enum", enumId);
        valuesArray.add(valueObject);
        jsonObject.add("values", valuesArray);
        return jsonObject;
    }

    private JsonArray getContactCustomFields(Card card) {
        JsonArray customFields = new JsonArray();
        customFields.add(getJsonElementWithNameAndValue(81529, 168171, card.getOwnerPhone()));
        customFields.add(getJsonElementWithNameAndValue(81531, 168183, card.getOwnerEmail()));
        return customFields;
    }

    public String prepareContact(Card card, AmoCrmResponsibleUser responsibleUser) {
        JsonObject rootElement = new JsonObject();
        JsonArray dealArray = new JsonArray();
        rootElement.add("add", dealArray);

        JsonObject dealElement = new JsonObject();
        dealArray.add(dealElement);

        dealElement.addProperty("name", card.getOwnerSurname() + " " + card.getOwnerName() + " " + card.getOwnerPatronymic());
        dealElement.addProperty("created_at", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        dealElement.addProperty("updated_at", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        dealElement.addProperty("responsible_user_id", "2274931");
        dealElement.add("custom_fields", getContactCustomFields(card));

        return new Gson().toJson(rootElement);
    }
}
