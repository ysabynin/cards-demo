package com.ysabynin.diagnosticcard.core.service.notification;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
    void sendComplexMessage(String to, String subject, String text, String pathToAttachment);
}
