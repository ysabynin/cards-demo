package com.ysabynin.diagnosticcard.core.entity;

public enum CardStatus {
    CREATED,
    VERIFIED,
    PAID
}
