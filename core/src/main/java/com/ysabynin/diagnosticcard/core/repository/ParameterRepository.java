package com.ysabynin.diagnosticcard.core.repository;

import com.ysabynin.diagnosticcard.core.entity.Parameter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ParameterRepository extends CrudRepository<Parameter, Long> {
    List<Parameter> findAll();

    Parameter findByName(String name);
}

