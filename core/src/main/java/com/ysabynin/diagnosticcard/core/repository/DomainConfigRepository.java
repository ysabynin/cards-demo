package com.ysabynin.diagnosticcard.core.repository;

import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DomainConfigRepository extends CrudRepository<DomainConfig, Long> {
    DomainConfig findFirstByDomainContaining(String domain);
}

