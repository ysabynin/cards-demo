package com.ysabynin.diagnosticcard.core.repository;

import com.ysabynin.diagnosticcard.core.entity.Card;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


@Repository
public interface CardRepository extends CrudRepository<Card, Long> {
    List<Card> findAll();

    Card findByOwnerEmail(String email);

    Card findOneByTsVinAndTsBodyAndTsFrameAndCreationDate(String tsVin, String tsBody, String tsFrame, Date creationDate);
}

