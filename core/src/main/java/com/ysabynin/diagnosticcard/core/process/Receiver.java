package com.ysabynin.diagnosticcard.core.process;

import com.ysabynin.diagnosticcard.core.pojo.CreateCardTask;
import com.ysabynin.diagnosticcard.core.repository.CityRepository;
import com.ysabynin.diagnosticcard.core.repository.ParameterRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class Receiver {
    @Autowired
    ProtoAutoService protoStrategy;

    @Autowired
    DcardAutoService dcardStrategy;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    MessageConverter messageConverter;

    @RabbitListener(queues = "cards")
    public void receiveMessage(Message message) {
        log.info("Card has been read from the queue");
        Object object = messageConverter.fromMessage(message);
        if (object instanceof CreateCardTask) {
            CreateCardTask task = (CreateCardTask) object;
            AppMode appMode = task.getAppMode();
            if (appMode == null) {
                log.info("No mode defined!");
            }
            if (AppMode.PROTO.equals(appMode)) {
                protoStrategy.launch((CreateCardTask) object);
            } else {
                dcardStrategy.launch((CreateCardTask) object);
            }
        }
    }
}
