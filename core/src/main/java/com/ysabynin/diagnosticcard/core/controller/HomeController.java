package com.ysabynin.diagnosticcard.core.controller;

import com.google.gson.Gson;
import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import com.ysabynin.diagnosticcard.core.repository.CardRepository;
import com.ysabynin.diagnosticcard.core.service.DomainService;
import com.ysabynin.diagnosticcard.core.service.notification.NotificationService;
import com.ysabynin.diagnosticcard.core.service.ParameterService;
import com.ysabynin.diagnosticcard.core.service.PriceService;
import com.ysabynin.diagnosticcard.core.utils.RequestUtils;
import com.ysabynin.diagnosticcard.core.service.amo.BaseAmoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Slf4j
public class HomeController {
    @Autowired
    CardRepository cardRepository;

    @Autowired
    ParameterService parameterService;

    @Autowired
    BaseAmoService baseAmoService;

    @Autowired
    DomainService domainService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    PriceService priceService;

    @ModelAttribute
    public void addAttributes(Model model) {
        Map<String, String> map = parameterService.getRequiredParams();
        model.addAttribute("parameters", map);
        model.addAttribute("cardPrice", priceService.getUsualPrice());
        model.addAttribute("appMode", parameterService.getAppMode().toString().toLowerCase());
    }

    @RequestMapping(value = "/handlePayment", method = RequestMethod.GET)
    public String homeRequest() {
        return "redirect:/payment";
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public String paymentRequest(HttpServletRequest request, Model model, @RequestParam(value = "id", required = false) Long id) {
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        if (id == null || domainConfig == null || StringUtils.isEmpty(domainConfig.getMode())) {
            model.addAttribute("hideSales", true);
            return "welcome";
        }
        Card card = cardRepository.findOne(id);

        log.info("Process payment, cardId = " + id + "card body = " + card);
        model.addAttribute("legalAddress", domainConfig.getLegalAddress());
        model.addAttribute("toAddress", domainConfig.getToAddress());
        model.addAttribute("hideSales", false);
        model.addAttribute("mode", domainConfig.getMode());
        if (card != null) {
            model.addAttribute("cardPrice", card.getPrice());
            model.addAttribute("data", new Gson().toJson(card));
        }
        return "welcome";
    }

    @RequestMapping("/home")
    public String homeRequest(Model model, HttpServletRequest request) {
        model.addAttribute("hideSales", false);
        model.addAttribute("mode", "sales");
        model.addAttribute("cardPrice", priceService.calculatePriceForGoogle(request));

        if (domainService.isDonateDomain()) {
            return "redirect:/donate";
        }
        return "welcome";
    }

    @RequestMapping(value = {"/*", "/welcome"})
    public String pageForYandex(Model model, HttpServletRequest request) {
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        if (domainConfig == null || StringUtils.isEmpty(domainConfig.getMode())) {
            model.addAttribute("hideSales", true);
            return "welcome";
        }
        model.addAttribute("legalAddress", domainConfig.getLegalAddress());
        model.addAttribute("toAddress", domainConfig.getToAddress());
        model.addAttribute("mode", domainConfig.getMode());
        model.addAttribute("cardPrice", priceService.calculatePriceForYandex(request));

        if (domainService.isDonateDomain()) {
            return "redirect:/donate";
        }
        String mode = domainConfig.getMode();
        if (StringUtils.isEmpty(mode) || "water-yandex".equals(mode)) {
            model.addAttribute("hideSales", true);
            return "welcome";
        } else if ("sales".equals(mode)) {
            model.addAttribute("hideSales", false);
            return "welcome";
        } else if (mode.contains("water-help")) {
            model.addAttribute("hideSales", true);
            model.addAttribute("disableSending", true);
            return "welcome";
        }

        return "welcome";
    }
}
