package com.ysabynin.diagnosticcard.core.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class DomainConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String domain;
    private String legalAddress;
    private String toAddress;
    private String mode;
    private Integer yandexPrice;
    private Integer googlePrice;
    private String amoConfig;
    private String comment;

    @OneToOne
    private Account account;
}
