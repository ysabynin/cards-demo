package com.ysabynin.diagnosticcard.core.repository;

import com.ysabynin.diagnosticcard.core.entity.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CityRepository extends CrudRepository<City, Long> {
    List<City> findAll();
}

