package com.ysabynin.diagnosticcard.core.service;

import com.ysabynin.diagnosticcard.core.entity.Account;
import com.ysabynin.diagnosticcard.core.process.AppMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DefaultAccountService {
    @Value("${proto.login}")
    String protoLogin;

    @Value("${proto.password}")
    String protoPasssword;

    @Value("${proto.login}")
    String dcardLogin;

    @Value("${proto.password}")
    String dcardPassword;

    public Account getDefaultAccount(AppMode appMode) {
        if (AppMode.PROTO == appMode) {
            return new Account(protoLogin, protoPasssword, null);
        } else {
            return new Account(dcardLogin, dcardPassword, null);
        }
    }
}
