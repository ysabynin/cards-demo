package com.ysabynin.diagnosticcard.core.service;

import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import com.ysabynin.diagnosticcard.core.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class PriceService {
    @Value("${card.discount.price}")
    Integer discountPrice;

    @Value("${card.usual.price}")
    Integer usualPrice;

    @Autowired
    DomainService domainService;

    public Integer calculatePriceForGoogle(HttpServletRequest request) {
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        boolean isFromSeo = RequestUtils.isFromSeo(request);
        Integer googlePrice = domainConfig == null || domainConfig.getGooglePrice() == null ?
                discountPrice : domainConfig.getGooglePrice();
        return isFromSeo ? usualPrice : googlePrice;
    }

    public Integer calculatePriceForYandex(HttpServletRequest request) {
        DomainConfig domainConfig = domainService.getCurDomainConfig(request);
        return RequestUtils.isFromSeo(request) ? usualPrice : domainConfig.getYandexPrice();
    }

    public Integer getUsualPrice() {
        return usualPrice;
    }
}
