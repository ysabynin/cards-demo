package com.ysabynin.diagnosticcard.core.service.amo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BaseAmoService {
    protected static final String API_PREFIX = "https://oformlenie.amocrm.ru/";

    @Autowired
    protected RestTemplate restTemplate;

    public ResponseEntity login() {
        Map<String, String> map = new HashMap() {{
            put("USER_LOGIN", "online.card1@yandex.ru");
            put("USER_HASH", "c38f4560abe135b3891e546d894206e8");
        }};
        HttpEntity requestEntity = new HttpEntity(map);
        return restTemplate.exchange(
                API_PREFIX + "private/api/auth.php?type=json",
                HttpMethod.POST,
                requestEntity,
                Object.class);
    }

    public ResponseEntity getAccount(List<String> cookies) {
        HttpHeaders httpHeaders = new HttpHeaders();
        cookies.forEach(cookie -> httpHeaders.add("Cookie", cookie));
        HttpEntity requestEntity = new HttpEntity(null, httpHeaders);
        return restTemplate.exchange(
                API_PREFIX + "api/v2/account?with=users,pipelines,groups,note_types,task_types",
                HttpMethod.GET,
                requestEntity,
                Object.class);
    }
}
