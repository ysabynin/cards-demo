package com.ysabynin.diagnosticcard.core.api.payment;

import com.ysabynin.diagnosticcard.core.repository.CardRepository;
import com.ysabynin.diagnosticcard.core.service.ParameterService;
import com.ysabynin.diagnosticcard.core.service.amo.DealJsonService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public abstract class BasePaymentController {
    @Autowired
    DealJsonService dealJsonService;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    ParameterService parameterService;

    String compileLogRecord(String message, HttpServletRequest request, Map<String, String> parametersMap) {
        return message + ": " + "HttpServletRequest={ IP:" + request.getRemoteAddr() + "} "
                + "Parameters=" + parametersMap;
    }

    Map<String, String> createParametersMap(HttpServletRequest req) {
        Map<String, String> parametersMap = new HashMap<>();
        Enumeration<String> parameterNames = req.getParameterNames();

        while (parameterNames.hasMoreElements()) {
            String paramName = parameterNames.nextElement();
            parametersMap.put(paramName, req.getParameter(paramName));
        }
        return parametersMap;
    }

    abstract void handlePayment(HttpServletRequest request);
}
