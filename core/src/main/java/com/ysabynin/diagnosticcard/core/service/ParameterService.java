package com.ysabynin.diagnosticcard.core.service;

import com.ysabynin.diagnosticcard.core.entity.Parameter;
import com.ysabynin.diagnosticcard.core.pojo.CardCreationMode;
import com.ysabynin.diagnosticcard.core.process.AppMode;
import com.ysabynin.diagnosticcard.core.repository.ParameterRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ParameterService {
    private static final List<String> requiredParams = Arrays.asList(
            "yandex.metric.ind",
            "card.process.mode",
            "target.platform",
            "payment.mode",
            "yandex.money.address",
            "intellect.money.form",
            "intellect.money.account",
            "donate.intellect.money.form",
            "donate.intellect.money.account"
    );

    @Autowired
    ParameterRepository parameterRepository;

    public String getParamByName(String name) {
        Parameter parameter = parameterRepository.findByName(name);
        if (parameter == null || parameter.getValue() == null)
            return null;

        return parameter.getValue();
    }

    public Map<String, String> getRequiredParams() {
        return parameterRepository.findAll().stream()
                .filter(p -> requiredParams.contains(p.getName()))
                .collect(Collectors.toMap(Parameter::getName, Parameter::getValue));
    }

    public CardCreationMode getCardCreationMode() {
        String processMode = getParamByName("card.process.mode");
        if (StringUtils.isEmpty(processMode))
            return null;

        return CardCreationMode.valueOf(processMode.toUpperCase());
    }

    public AppMode getAppMode() {
        String targetPlatform = getParamByName("target.platform");
        return targetPlatform != null ? AppMode.valueOf(targetPlatform.toUpperCase()) : AppMode.PROTO;
    }
}
