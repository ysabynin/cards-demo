package com.ysabynin.diagnosticcard.core.service;

import com.ysabynin.diagnosticcard.core.entity.DomainConfig;
import com.ysabynin.diagnosticcard.core.repository.DomainConfigRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
@Slf4j
public class DomainService {
    @Autowired
    DomainConfigRepository domainConfigRepository;

    @Autowired
    ParameterService parameterService;

    public DomainConfig getCurDomainConfig(HttpServletRequest request) {
        String domain = request.getHeader("host");
        if (StringUtils.isEmpty(domain)) {
            return null;
        }
        return domainConfigRepository.findFirstByDomainContaining(domain);
    }

    public boolean isDonateDomain() {
        String donateDomain = parameterService.getParamByName("donate.domain");
        if (StringUtils.isEmpty(donateDomain))
            return false;

        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            log.info("hostAddress=" + ip.getHostAddress());
            return donateDomain.contains(ip.getHostAddress());
        } catch (UnknownHostException e) {
            return false;
        }
    }
}
