package com.ysabynin.diagnosticcard.core.api.payment;

import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.service.amo.DealStep;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Slf4j
public class YandexController extends BasePaymentController {
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/pay", method = RequestMethod.POST)
    @Override
    public void handlePayment(HttpServletRequest request) {
        Map<String, String> parametersMap = createParametersMap(request);
        log.info("Recieved yandex notification: " + parametersMap);
        String withdrawAmount = parametersMap.get("withdraw_amount");
        String label = parametersMap.get("label");

        if (StringUtils.isEmpty(withdrawAmount) || StringUtils.isEmpty(label)) {
            log.info("Required parameters(withdraw_amount, label) were not specified");
            return;
        }

        double price = Double.parseDouble(withdrawAmount);
        log.info("Withdraw amount = " + price);
        if (price < 300) {
            log.info("Payment operation was not added to CRM due price = " + price);
            return;
        }

        if (request.getParameter("test_notification") != null) {
            log.info("Test notification is recieved");
        }

        String secret = parameterService.getParamByName("yandex.money.secret");
        if (StringUtils.isEmpty(secret) || !YandexUtils.isHashValid(parametersMap, secret)) {
            log.error(compileLogRecord("SHA-1 hash verification failed", request, parametersMap));
            return;
        }

        Card card = cardRepository.findOne(Long.valueOf(label));
        if (card != null) {
            log.info("Information about client is founded in database");
            dealJsonService.updateDealStep(card, price, DealStep.PAID);
        } else {
            log.info("Information about client is not founded in database");
        }
    }
}
