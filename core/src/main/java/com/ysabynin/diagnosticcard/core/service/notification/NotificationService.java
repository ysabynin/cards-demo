package com.ysabynin.diagnosticcard.core.service.notification;

import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.pojo.CardCreationMode;

import java.util.Map;

public interface NotificationService {
    void notifyOwners(Card card, Map<String, String> cardLabelValueMap, String errorMessage);

    void notifyDeveloperAboutUnparsedCities();

    void notifyClient(Card card, String pathToFile);

    void notifyManagerAboutApplication(Card card, CardCreationMode creationMode);

    void notifyAboutSuccessfullyProcessedCard(Map<String, String> cardLabelValueMap, String pathToFile);

    void notifyClientAboutLongTimeProcessedCard(Card card);

    void notifyManagerAboutUnprocessedCard(Card card, String errorMessage);

    void notifyManagerAboutLongTimeProcessedCard(Map<String, String> cardLabelValueMap);

    void notifyManagerAboutCallback(Map<String, String> callback);

    void notifyManagerAboutPayment(Map<String, String> callback);

    void notifyManagerAboutQuestion(Map<String, String> callback);
}
