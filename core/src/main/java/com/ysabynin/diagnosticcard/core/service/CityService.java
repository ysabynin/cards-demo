package com.ysabynin.diagnosticcard.core.service;

import com.ysabynin.diagnosticcard.core.entity.City;
import com.ysabynin.diagnosticcard.core.pojo.ComboboxItem;
import com.ysabynin.diagnosticcard.core.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityService {
    @Autowired
    CityRepository cityRepository;

    public List<ComboboxItem> getOptions() {
        return cityRepository.findAll().stream()
                .sorted(Comparator.comparing(City::getName))
                .map(e -> new ComboboxItem(e.getName(), e.getName()))
                .collect(Collectors.toList());
    }
}
