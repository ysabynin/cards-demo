package com.ysabynin.diagnosticcard.core.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    private String description;
    private String disabledRegions;

    public Account(String login, String password, String description) {
        this.login = login;
        this.password = password;
        this.description = description;
    }
}
