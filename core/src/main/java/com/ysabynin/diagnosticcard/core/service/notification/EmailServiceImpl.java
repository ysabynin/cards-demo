package com.ysabynin.diagnosticcard.core.service.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    public JavaMailSender emailSender;

    @Value("${mail.sender}")
    private String sender;

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        String[] toArray = to.split(",");
        message.setFrom(sender);
        message.setTo(toArray);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    @Override
    public void sendComplexMessage(String to, String subject, String text, String pathToAttachment) {
        String[] toArray = to.split(",");
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            message.setFrom(sender);
            helper.setTo(toArray);
            helper.setSubject(subject);
            helper.setText(text, true);
            if (pathToAttachment != null) {
                FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
                helper.addAttachment("card.pdf", file);
            }
            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
