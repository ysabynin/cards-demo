package com.ysabynin.diagnosticcard.core.pojo;

import com.ysabynin.diagnosticcard.core.entity.Account;
import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.process.AppMode;

public class CreateCardTask {
    private Card card;
    private Account account;
    private AppMode appMode;

    public CreateCardTask() {
    }

    public CreateCardTask(Card card, Account account, AppMode appMode) {
        this.card = card;
        this.account = account;
        this.appMode = appMode;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public AppMode getAppMode() {
        return appMode;
    }

    public void setAppMode(AppMode appMode) {
        this.appMode = appMode;
    }

    @Override
    public String toString() {
        return "CreateCardTask{" +
                "card=" + card +
                ", account=" + account +
                ", appMode=" + appMode +
                '}';
    }
}


