package com.ysabynin.diagnosticcard.core.service.amo;

public enum DealStep {
    FIRST_APPEAL("18947197"),
    EXECUTED("18947194"),
    PAID("18947200"),
    NOT_PAID("18977341");

    private String id;

    DealStep(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
