package com.ysabynin.diagnosticcard.core.pojo;

public enum CardCreationMode {
    AUTO("auto"),
    CONFIRM("confirm");

    private String text;

    CardCreationMode(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
