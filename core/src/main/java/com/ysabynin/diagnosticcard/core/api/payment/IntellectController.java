package com.ysabynin.diagnosticcard.core.api.payment;

import com.ysabynin.diagnosticcard.core.entity.Card;
import com.ysabynin.diagnosticcard.core.service.amo.DealStep;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@Slf4j
public class IntellectController extends BasePaymentController {
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/notifyPayment", method = RequestMethod.POST)
    @Override
    public void handlePayment(HttpServletRequest request) {
        Map<String, String> parametersMap = createParametersMap(request);
        log.info("Recieved intellect money notification: " + parametersMap);
        String paymentStatus = parametersMap.get("paymentStatus");
        String userEmail = parametersMap.get("userEmail");

        log.info("PaymentStatus: " + paymentStatus + ", email: " + userEmail);
        if ("5".equals(paymentStatus) && !StringUtils.isEmpty(userEmail)) {
            log.info("Card is find and payed. Deal should be added to AMO");
            Card card = cardRepository.findByOwnerEmail(userEmail);
            dealJsonService.updateDealStep(card, Double.valueOf(card.getPrice()), DealStep.PAID);
        }
    }
}
